#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include "bmp_io_with_messages.h"
#include "image.h"
#include "sepia.h"
#include "displayer.h"

char const * const INPUT_FILE = "images/input.bmp";
char const * const OUTPUT_FILE = "images/output.bmp";
char const * const OUTPUT_FILE_ASM = "images/output_asm.bmp";

#define MAX_TOTAL_ERROR 6

int to_bmp_close_file_and_print_errors(char const * const output, struct image const * const img) {
    FILE * output_file = fopen(output, "wb");

    enum write_status writing_result = to_bmp(output_file, img);

    if (output_file && fclose(output_file) != 0) {
        display_error("IOError: output file was not successfully closed. Exit...");
        return 7;
    }

    if (writing_result != WRITE_OK) {
        display_error(write_errors_messages[writing_result]);
        return 8;
    }

    display_message(write_errors_messages[writing_result]);

    return 0;
}

void test_images_equals(
    char const * const input,
    char const * const output_c,
    char const * const output_sse
) {
    FILE * input_file = fopen(input, "rb");
    struct image img = {0};

    display_message_without_line_break("Reading input file... ");

    enum read_status reading_result = from_bmp(input_file, &img);

    if (input_file && fclose(input_file) != 0) {
        display_error("Warning: input file was not successfully closed");
    }

    if (reading_result != READ_OK) {
        display_error(read_errors_messages[reading_result]);
        exit(4);
    }

    display_message(read_errors_messages[reading_result]);

    struct image img_asm = image_copy(img);

    display_message_without_line_break("Аpplying the filters to images... ");

    image_do_sepia_filter(img);
    image_do_sepia_filter_asm(img_asm);

    display_message("Done");

    display_message_without_line_break("Saving the image C... ");
    int saving_result = to_bmp_close_file_and_print_errors(output_c, &img);

    if (saving_result != 0) {
        image_free(&img);
        image_free(&img_asm);
        exit(saving_result);
    }

    display_message_without_line_break("Saving the image SSE... ");
    saving_result = to_bmp_close_file_and_print_errors(output_sse, &img_asm);

    if (saving_result != 0) {
        image_free(&img);
        image_free(&img_asm);
        exit(saving_result);
    }

    bool is_equals = image_is_equals(&img, &img_asm, MAX_TOTAL_ERROR);

    image_free(&img);
    image_free(&img_asm);

    if (!is_equals) {
        display_error("The images are not equal. Test failed");
        exit(5);
    }

    display_message("The images are equal. Test passed");
}

#define NUMBER_OF_ITERATIONS 10

void test_speed(char const * const input) {
    FILE * input_file = fopen(input, "rb");
    struct image img = {0};

    display_message_without_line_break("Reading input file... ");

    enum read_status reading_result = from_bmp(input_file, &img);

    if (input_file && fclose(input_file) != 0) {
        display_error("Warning: input file was not successfully closed");
    }

    if (reading_result != READ_OK) {
        display_error(read_errors_messages[reading_result]);
        exit(4);
    }

    display_message(read_errors_messages[reading_result]);

    struct image img_asm = image_copy(img);

    display_message_without_line_break("Аpplying the filters to images... ");

    struct rusage usage;
    struct timeval start, end;

    getrusage(RUSAGE_SELF, &usage);
    start = usage.ru_utime;

    for (uint64_t i = 0; i < NUMBER_OF_ITERATIONS; ++i) {
        image_do_sepia_filter(img);
    }

    getrusage(RUSAGE_SELF, &usage);
    end = usage.ru_utime;

    double c_time = (double) (end.tv_usec - start.tv_usec) / NUMBER_OF_ITERATIONS;

    getrusage(RUSAGE_SELF, &usage);
    start = usage.ru_utime;

    for (uint64_t j = 0; j < NUMBER_OF_ITERATIONS; ++j) {
        image_do_sepia_filter_asm(img_asm);
    }

    getrusage(RUSAGE_SELF, &usage);
    end = usage.ru_utime;

    double asm_time = (double) (end.tv_usec - start.tv_usec) / NUMBER_OF_ITERATIONS;

    display_message("Done");
    display_message_without_line_break("Number of iterations: ");
    printf("%u\n", NUMBER_OF_ITERATIONS);

    display_message_without_line_break("Average time for C: ");
    printf("%lf mcs\n", c_time);

    display_message_without_line_break("Average time for Assembler: ");
    printf("%lf mcs\n", asm_time);

    image_free(&img);
    image_free(&img_asm);

    if (c_time <= asm_time) {
        display_error("Test failed");
        exit(6);
    }

    display_message("Test passed");
}

int main(int argc, char** argv) {
    if (argc > 4) {
        display_error("Too many arguments, expected names of input, output_c and output_sse files");
        exit(1);
    }

    char const * input = (argc < 2) ? INPUT_FILE : argv[1];
    char const * output_c = (argc < 3) ? OUTPUT_FILE : argv[2];
    char const * output_sse = (argc < 4) ? OUTPUT_FILE_ASM : argv[3];

    display_message("TEST 1. EQUALITY");
    test_images_equals(input, output_c, output_sse);
    display_message("\nTEST 2. SPEED");
    test_speed(input);

    return 0;
}
