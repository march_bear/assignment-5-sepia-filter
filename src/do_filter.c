#include <stdlib.h>

#include "do_filter.h"
#include "image.h"

void image_do_filter(struct image source, pixel_do_filter filter) {
    for (uint64_t i = 0; i < source.height; ++i) {
        for (uint64_t j = 0; j < source.width; ++j) {
            filter(get_pixel(source, i, j));
        }
    }
}
