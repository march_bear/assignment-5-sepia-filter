#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_io.h"
#include "bmp_header.h"
#include "image.h"

#define ONE_BYTE 1
#define COMPRESSION_BI_RGB 0

static uint8_t getPadding(uint64_t width) {
    return ((PIXEL_SIZE * width) % 4 == 0) ? 0 : 4 - (PIXEL_SIZE * width) % 4;
}

static enum read_status check_read_header(struct bmp_header const header) {
    if (header.bfType != 0x4D42) return READ_INVALID_SIGNATURE;
    if (header.bfReserved != 0x0) return READ_INVALID_HEADER;
    if ((size_t) header.bOffBits < sizeof(struct bmp_header)) return READ_INVALID_HEADER;
    if (header.biCompression != COMPRESSION_BI_RGB) return READ_INVALID_COMPRESSION;
    if (header.biPlanes != 1) return READ_INVALID_HEADER;
    if (header.biBitCount != PIXEL_SIZE * 8) return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    if (in == NULL)
        return READ_INVALID_FILE_POINTER;

    struct bmp_header header = {0};

    if (fread(&header, ONE_BYTE, sizeof(struct bmp_header), in) != sizeof(struct bmp_header)) {
        return READ_INVALID_HEADER;
    }

    enum read_status check_read_header_result = check_read_header(header);
    if (check_read_header_result != READ_OK) {
        return check_read_header_result;
    }

    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = malloc(img->height * img->width * PIXEL_SIZE);

    uint8_t const padding = getPadding(img->width);

    for (uint64_t i = img->height; i-- > 0 ;) {
        uint64_t number_of_bytes_read = fread((img->data + i * img->width), PIXEL_SIZE, img->width, in);

        if (number_of_bytes_read != img->width) {
            return READ_INVALID_IMAGE;
        }

        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_INVALID_IMAGE;
        }
    }

    return READ_OK;
}

static struct bmp_header create_header(struct image const * img) {
    return (struct bmp_header) {
        .bfType = 0x4D42,
        .bfileSize = img->height * (PIXEL_SIZE * img->width + getPadding(img->width)) + 2 + 54,
        .bfReserved = 0x0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 0x1,
        .biBitCount = PIXEL_SIZE * 8,
        .biCompression = COMPRESSION_BI_RGB,
        .biSizeImage = img->height * (PIXEL_SIZE * img->width + getPadding(img->width)) + 2,
        .biXPelsPerMeter = img->width,
        .biYPelsPerMeter = img->height,
        .biClrUsed = 0x0,
        .biClrImportant = 0x0,
    };
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (out == NULL)
        return WRITE_INVALID_FILE_POINTER;

    struct bmp_header header = create_header(img);

    if (fwrite(&header, ONE_BYTE, sizeof(struct bmp_header), out) != sizeof(struct bmp_header))
        return WRITE_HEADER_ERROR;

    uint8_t const padding = getPadding(img->width);
    uint32_t null_byte = 0;

    for (uint64_t i = img->height; i-- > 0 ;) {
        uint64_t number_of_bytes_written = fwrite((img->data + i * img->width), PIXEL_SIZE, img->width, out);
        number_of_bytes_written += fwrite(&null_byte, ONE_BYTE, padding, out);

        if (number_of_bytes_written != img->width + padding) {
            return WRITE_ERROR;
        }
    }

    uint64_t number_of_bytes_written = fwrite(&null_byte, ONE_BYTE, 1, out);
    number_of_bytes_written += fwrite(&null_byte, ONE_BYTE, 1, out);

    if (number_of_bytes_written != 2) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
