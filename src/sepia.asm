%macro pack_byte 2
    xor rax, rax
    movzx eax, byte [%1]
    cvtsi2ss %2, eax
    shufps %2, %2, 0
%endmacro

section .rodata
    bc: dd 0.131, 0.168, 0.189, 0
    gc: dd 0.534, 0.686, 0.769, 0
    rc: dd 0.272, 0.349, 0.393, 0

section .text

global pixel_do_sepia_filter_asm

pixel_do_sepia_filter_asm:
    ; т. к. во время установки новых значений мы заденем байт b следующего пикселя, его необходимо сохранить
    mov r8, [rdi + 3]

    pack_byte rdi, xmm0
    pack_byte rdi + 1, xmm1
    pack_byte rdi + 2, xmm2

    movups xmm3, [bc]
    movups xmm4, [gc]
    movups xmm5, [rc]

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    ; преобразуем упакованные числа с плавающей точкой в целочисленные значения
    cvtps2dq xmm0, xmm0
    ; упаковка двойных слов в слова c насыщением (если числа были больше 0xFFFF, то они будут преобразованы в 0xFFFF)
    packusdw xmm0, xmm0
    ; упаковка слов в байты с насыщением
    packuswb xmm0, xmm0

    movd [rdi], xmm0
    mov [rdi + 3], r8
    ret
