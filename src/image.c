#include <stdlib.h>
#include <stdio.h>

#include "image.h"

struct image image_copy(struct image const source) {
    struct image copy = { 0 };
    copy.height = source.height;
    copy.width = source.width;
    copy.data = malloc(copy.height * copy.width * PIXEL_SIZE);

    for (uint64_t i = 0; i < copy.height; ++i) {
        for (uint64_t j = 0; j < copy.width; ++j) {
            copy.data[i * copy.width + j] = *get_pixel(source, i, j);
        }
    }

    return copy;
}

_Bool pixel_is_equals(struct pixel const * const pxl1, struct pixel const * const pxl2, uint16_t max_total_error) {
    return abs(pxl1->b - pxl2->b) + abs(pxl1->g - pxl2->g) + abs(pxl1->r - pxl2->r) <= max_total_error;
}

_Bool image_is_equals(struct image const * const img1, struct image const * const img2, uint16_t max_total_error) {
    if (img1->width != img2->width || img1->height != img2->height)
        return 0;

    uint64_t counter = 0;
    uint64_t err_counter = 0;
    for (uint64_t i = 0; i < img1->height; ++i) {
        for (uint64_t j = 0; j < img1->width; ++j) {
            counter++;
            if (!pixel_is_equals(get_pixel(*img1, i, j), get_pixel(*img2, i, j), max_total_error)) {
                return 0;
            }
        }
    }

    return 1;
}

struct pixel * get_pixel(struct image source, uint64_t i, uint64_t j) {
    return source.data + i * source.width + j;
}

void image_free(struct image * source) {
    free(source->data);
    source->data = NULL;
    source->width = 0;
    source->height = 0;
}
