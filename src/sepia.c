#include <inttypes.h>
#include <stdio.h>

#include "do_filter.h"
#include "image.h"

extern void pixel_do_sepia_filter_asm(struct pixel * pxl);

static uint8_t get_color(int r, int g, int b, float r_f, float g_f, float b_f) {
    int res = (r * r_f) + (g * g_f) + (b * b_f);

    return (res > 255) ? 255 : res;
}

void pixel_do_sepia_filter(struct pixel * pxl) {
    static const float c[3][3] = {
        { .393f, .769f, .189f },
        { .349f, .686f, .168f },
        { .272f, .543f, .131f }
    };

    uint8_t r = get_color(pxl->r, pxl->g, pxl->b, c[0][0], c[0][1], c[0][2]);
    uint8_t g = get_color(pxl->r, pxl->g, pxl->b, c[1][0], c[1][1], c[1][2]);
    uint8_t b = get_color(pxl->r, pxl->g, pxl->b, c[2][0], c[2][1], c[2][2]);

    pxl->r = r;
    pxl->g = g;
    pxl->b = b;
}

void image_do_sepia_filter(struct image const source) {
    image_do_filter(source, pixel_do_sepia_filter);
}

void image_do_sepia_filter_asm(struct image const source) {
    image_do_filter(source, pixel_do_sepia_filter_asm);
}