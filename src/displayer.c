#include <stdio.h>

void display_message(char const * const msg) {
    printf("%s\n", msg);
}

void display_error(char const * const msg) {
    fprintf(stderr, "%s\n", msg);
}

void display_message_without_line_break(char const * const msg) {
    printf("%s", msg);
}
