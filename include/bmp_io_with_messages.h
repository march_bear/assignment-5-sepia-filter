#ifndef R_ANGLE_WITH_MESSAGES_H
#define R_ANGLE_WITH_MESSAGES_H

#include "bmp_io.h"

char * const read_errors_messages[] = {
    [READ_OK] = "Done",
    [READ_INVALID_SIGNATURE] = "Read error: invalid signature. A bmp file is expected",
    [READ_INVALID_BITS] = "Read error: invalid bits. Only 24-bit bmp files are supported",
    [READ_INVALID_HEADER] = "Read error: invalid input file header",
    [READ_INVALID_COMPRESSION] = "Read error: invalid value of biCompression attribute of the bmp file header",
    [READ_INVALID_FILE_POINTER] = "Read error: failed to open input file. Maybe it doesn't exist",
    [READ_INVALID_IMAGE] = "Read error: Invalid image array. The file may be corrupted",
};

char * const write_errors_messages[] = {
    [WRITE_OK] = "Done",
    [WRITE_ERROR] = "Write error: data could not be written to the output file",
    [WRITE_HEADER_ERROR] = "Write error: error during file header writing",
    [WRITE_INVALID_FILE_POINTER] = "failed to open output file"
};

#endif
