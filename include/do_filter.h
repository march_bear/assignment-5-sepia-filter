#ifndef DO_FILTER_H
#define DO_FILTER_H

#include "image.h"

typedef void (*pixel_do_filter)(struct pixel * pxl);

void image_do_filter(struct image source, pixel_do_filter filter);

#endif
