#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#define PIXEL_SIZE 3

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_copy(struct image const source);

_Bool pixel_is_equals(struct pixel const * const pxl1, struct pixel const * const pxl2, uint16_t max_total_error);

_Bool image_is_equals(struct image const * const img1, struct image const * const img2, uint16_t max_total_error);

struct pixel * get_pixel(struct image source, uint64_t i, uint64_t j);

void image_free(struct image * source);

#endif
