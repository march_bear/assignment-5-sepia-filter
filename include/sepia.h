#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

void pixel_do_sepia_filter(struct pixel * pxl);

void image_do_sepia_filter(struct image const source);

void image_do_sepia_filter_asm(struct image const source);

#endif
