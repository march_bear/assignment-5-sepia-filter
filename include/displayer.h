#ifndef DISPLAYER_H
#define DISPLAYER_H

void display_message(char const * const msg);

void display_error(char const * const msg);

void display_message_without_line_break(char const * const msg);

#endif
