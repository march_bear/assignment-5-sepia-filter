ASM=nasm
ASMFLAGS=-g -f elf64
LD=ld

CC=gcc
CFLAGS=-no-pie
INC_DIR=include

MAIN=main

.PHONY: run clean test

sepia.o: src/sepia.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

$(MAIN): src/$(MAIN).c src/bmp_io.c src/displayer.c src/do_filter.c src/image.c src/sepia.c sepia.o
	$(CC) $(CFLAGS) -I $(INC_DIR) -o $@ $^

test: clean run

clean:
	rm -rf *.o
	rm -rf $(MAIN)

run: $(MAIN)
	./$<
